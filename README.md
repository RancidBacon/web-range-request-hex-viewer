## Web Range-Request Hex Viewer

Make web server range-requests, see the binary response in a classic
Hex/ASCII view.

![Web Range-Request Hex Viewer Screenshot](web-range-request-hex-viewer-screenshot.png)

Run something like this command from the directory that
contains `index.html`:

```
python -m SimpleHTTPServer
```

Visit `http://localhost:8000/` in your browser.

Specify a URL and a starting byte and number of bytes to retrieve.

The server needs to permit Range Requests if CORS comes into play.

HTML sanitisation--what's that? :)

A quick hack from RancidBacon.com.

License: MIT
